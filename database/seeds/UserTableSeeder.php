<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'developer',
            'email' => 'admin@tuna.com',
            'password' => Hash::make('Test@Tuna123#')
        ]);
    }
}
