###Technical Assignment for PHP Developer###

1. composer install --no-dev
2. Copy .env.example to .env
3. php artisan key:generate
4. Update .env file with necessary details
5. php artisan migrate --seed
5. php artisan serve